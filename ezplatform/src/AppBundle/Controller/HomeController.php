<?php

namespace AppBundle\Controller;

use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\Core\MVC\Symfony\View\ContentView;

class HomeController extends CommonController
{
    /**
     * @param ContentView $view
     *
     * @return ContentView
     */
    public function fullAction(ContentView $view): ContentView
    {

        $view->addParameters([
            'items' => $this->itemListChild($view->getLocation()->id),
        ]);

        return $view;
    }
}
