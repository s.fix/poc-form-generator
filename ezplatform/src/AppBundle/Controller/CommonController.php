<?php

namespace AppBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\API\Repository\Values\Content\Location;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\Core\QueryType\QueryType;
use Pagerfanta\Pagerfanta;

abstract class CommonController extends Controller
{
    /**
     * Liste l'ensemble des enfants.
     *
     * @param Location             $location
     * @param string[]|string|null $typeIdentifier
     * @param int                  $page
     * @param int                  $maxPerPage
     *
     * @return Location[]|Pagerfanta
     */
    protected function itemListChild(int $parentLocationId, $typeIdentifier = null, int $page = 1, int $maxPerPage = 10000): Pagerfanta
    {
        /** @var QueryType $query */
        $queryType = $this->container->get('app.query_type.location_children');

        /** @var LocationQuery $query */
        $query = $queryType->getQuery([
            'typeIdentifier' => $typeIdentifier,
            'parentLocationId' => $parentLocationId,
        ]);

        return $this->get('app.location_paginator')->paginate($query, $page, $maxPerPage);
    }
}
