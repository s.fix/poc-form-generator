<?php

namespace AppBundle\QueryType;

use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;

/**
 * Class LocationChildrenQueryType.
 */
class LocationChildrenQueryType
{
    /**
     * @param array $parameters
     *
     * @return LocationQuery|\eZ\Publish\API\Repository\Values\Content\Query
     */
    public function getQuery(array $parameters = [])
    {
        $filters = [
            new Criterion\ParentLocationId($parameters['parentLocationId']),
            new Criterion\Visibility(Criterion\Visibility::VISIBLE),
        ];

        if (array_key_exists('excludeTypeIdentifier', $parameters) && !empty($parameters['excludeTypeIdentifier'])) {
            $filters[] = new Criterion\LogicalNot(
                new Criterion\ContentTypeIdentifier($parameters['excludeTypeIdentifier'])
            );
        }

        if (array_key_exists('typeIdentifier', $parameters) && !empty($parameters['typeIdentifier'])) {
            $filters[] = new Criterion\ContentTypeIdentifier($parameters['typeIdentifier']);
        }

        $query = new LocationQuery();
        $query->filter = new Criterion\LogicalAnd($filters);
        $query->sortClauses = [
            new SortClause\Location\Priority(),
            new SortClause\ContentName(),
        ];

        if (array_key_exists('sortDateModified', $parameters) && !empty($parameters['sortDateModified'])) {
            array_unshift(
                $query->sortClauses,
                new SortClause\DateModified($parameters['sortDateModified'])
            );
        }

        return $query;
    }

    public function getName()
    {
        return self::class;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return array
     */
    public function getSupportedParameters(): array
    {
        return ['excludeTypeIdentifier', 'typeIdentifier', 'parentLocationId', 'sortDateModified'];
    }
}
