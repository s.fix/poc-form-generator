<?php

namespace AppBundle\QueryType;

use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;

/**
 * Class LocationChildrenQueryType.
 */
class LocationAllInTreeQueryType
{
    /**
     * @param array $parameters
     *
     * @return LocationQuery|\eZ\Publish\API\Repository\Values\Content\Query
     */
    public function getQuery(array $parameters = [])
    {
        $filters = [
            new Criterion\Visibility(Criterion\Visibility::VISIBLE),
            new Criterion\ContentTypeIdentifier($parameters['typeIdentifier']),
        ];

        $query = new LocationQuery();
        $query->filter = new Criterion\LogicalAnd($filters);
        $query->sortClauses = [
            new SortClause\ContentName(),
        ];

        return $query;
    }

    public function getName()
    {
        return self::class;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return array
     */
    public function getSupportedParameters(): array
    {
        return ['typeIdentifier'];
    }
}
