<?php

namespace AppBundle\Service\Paginator;

use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\Core\Pagination\Pagerfanta\LocationSearchAdapter;
use eZ\Publish\Core\Repository\SearchService;
use Pagerfanta\Pagerfanta;

/**
 * Wrapper autour d'une LocationQuery pour encapsuler les résultats dans un objet PagerFanta.
 */
class LocationPaginator
{
    /**
     * @var SearchService
     */
    private $searchService;

    /**
     * LocationPaginator constructor.
     *
     * @param SearchService $searchService
     */
    public function __construct(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    /**
     * @param LocationQuery $query
     * @param int           $page
     * @param int           $maxPerPage
     *
     * @return Pagerfanta
     *
     * @throws \Pagerfanta\Exception\OutOfRangeCurrentPageException
     * @throws \Pagerfanta\Exception\NotIntegerMaxPerPageException
     * @throws \Pagerfanta\Exception\NotIntegerCurrentPageException
     * @throws \Pagerfanta\Exception\LessThan1MaxPerPageException
     * @throws \Pagerfanta\Exception\LessThan1CurrentPageException
     */
    public function paginate(LocationQuery $query, int $page = 1, int $maxPerPage = 10000)
    {
        $items = new Pagerfanta(
            new LocationSearchAdapter($query, $this->searchService)
        );
        $items->setCurrentPage($page);
        $items->setMaxPerPage($maxPerPage);

        return $items;
    }
}
